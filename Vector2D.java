
public class Vector2D {
	private float x;
	private float y;
	
	
	public Vector2D() {
		super();
	}

	public Vector2D(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Vector2D [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2D other = (Vector2D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}
	
	public Vector2D add(Vector2D b)
	{
		this.x+=b.x;
		this.y+=b.y;
		return this;
	}
	public static Vector2D add(Vector2D b, Vector2D c)
	{
		return new Vector2D(b.x+c.x, b.y+c.y);
	}
	
	public Vector2D mul(float a)
	{
		this.x*=a;
		this.y*=a;
		return this;
	}
	
	public float module()
	{
		return (float) Math.sqrt(Math.pow(this.x, 2) +Math.pow(this.y, 2));
	}
	
	public Vector2D normalize()
	{
		float temp=(float) Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
		this.x/=temp;
		this.y/=temp;
		return this;
	}

}
