

public class Kinematic {
	private Vector2D position;
	private float orientation;
	private Vector2D velocity;
	private float rotation;
	
	
	public Kinematic() {
		super();
	}

	public Kinematic(Vector2D position, float orientation, Vector2D velocity, float rotation) {
		super();
		this.position = position;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
	}

	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public float getOrientation() {
		return orientation;
	}

	public void setOrientation(float orientation) {
		this.orientation = orientation;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public float getRotation() {
		return rotation;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	

	public void update(SteeringOutput steering, float time)
	{	
		this.position.add(this.velocity.mul(time)).add(steering.getLinear().mul((float) (0.5 * time * time)));
		this.orientation +=this.rotation*time+0.5 * steering.getAngular() * time * time;
		this.velocity.add(steering.getLinear().mul(time));
		orientation += steering.getAngular() * time;
	}
	
	public float getNewOrientation(float currentOrientation,Vector2D velocity)
	{
		return 0;
	}
}
